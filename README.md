Para inicializar o front-end, bastar utilizar o comando `ng serve` no terminal e acessar `http://localhost:4200/`. Após qualquer alteração, a página será automaticamente atualizada com as alterações aplicadas.

Destaque dos arquivos:
- cliente.ts: Neste arquivo, eu defini um objeto com suas informações (referentes aos dados em data.json no back-end).
- cliente.service.ts: Utilização de métodos de serviço.
- app.component.html: utilizei <app-clientes-lista></app-clientes-lista> para chamar o componente clientes-lista (na pasta clientes-lista).
- clientes-lista.component.ts: Método de listagem dos clientes para ser utilizado em clientes-lista.component.html.
- clientes-lista.component.html: Neste arquivo tem o HTML para a listagem de clientes. Utilizo *ngFor para percorrer todos os clientes e imprimo em uma tabela. Utilizo o bootstrap para facilitar na estilização.

Implementado:
- A listagem de clientes foi implementada, utilizando o arquivo de data.json do back-end para captura dos dados e o bootstrap essencialmente para estilização.

Não implementados:
- Tela de visualização de cliente, com suas informações de lista de oportunidades.
- Uma interface de controle que permita alteração do status de oportunidades.