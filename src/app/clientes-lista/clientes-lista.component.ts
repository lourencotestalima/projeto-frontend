import { Component, OnInit } from '@angular/core';
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';

@Component({
  selector: 'app-clientes-lista',
  templateUrl: './clientes-lista.component.html',
  styleUrls: ['./clientes-lista.component.scss']
})

export class ClientesListaComponent implements OnInit {

  clientes: Array<Cliente>;
  constructor(private clienteService: ClienteService) {}

    ngOnInit(){
      this.listarTodosClientes();
    }

    listarTodosClientes(){
      this.clienteService.listarTodosClientes().subscribe(dados => this.clientes = dados); 
    }
}