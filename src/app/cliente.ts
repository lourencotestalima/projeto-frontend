export interface Cliente {
    name: string,
    email: string,
    isActive: boolean,
    phone: string,
    revenue: number,
    agreedTerms: boolean
}