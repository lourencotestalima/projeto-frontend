import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cliente } from './cliente';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  
  private readonly clientesUrl = "http://localhost:3000/lista";
  
  constructor(private http: HttpClient) { }

  listarTodosClientes(){
    return this.http.get<Cliente[]>(`${this.clientesUrl}`)
  }
}